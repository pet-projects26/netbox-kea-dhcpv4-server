from flask import Flask, request
from netbox import *
import hmac, hashlib, base64, os
from dotenv import load_dotenv

app = Flask(__name__)

def verify_webhook(data, hmac_header):

    load_dotenv(override=True)
    API_SECRET_KEY = os.getenv("API_SECRET_KEY")

    # Calculate HMAC
    digest = hmac.new(API_SECRET_KEY.encode('utf-8'), data, digestmod=hashlib.sha512).hexdigest()
    computed_hmac = base64.b64encode(digest.encode('utf-8'))
    computed_header = base64.b64encode(hmac_header.encode('utf-8'))
    
    return hmac.compare_digest(computed_hmac, computed_header)


@app.route('/', methods=['POST'])
def hook():
    
    extract(request.get_json())
    return '', 200

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")