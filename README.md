# netbox-kea-dhcpv4-server


<h1 align="center">Hi 👋, I'm Harry</h1>
<h3 align="center">A normal person from Ho Chi Minh City</h3>

- 🔭 I’m currently working on [netbox-kea-dhcpv4-server](https://gitlab.com/pet-projects26/netbox-kea-dhcpv4-server)

- 🌱 I’m currently learning **Netbox & Kea DHCP Server**

- 📫 How to reach me **ngogiahung567@gmail.com**

</p>

<h3 align="left">Languages and Tools:</h3>
<p align="center"> 
<a href="https://www.python.org/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/python/python-horizontal.svg" alt="linux" height="45"/> </a> 
<a href="https://flask.palletsprojects.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/pocoo_flask/pocoo_flask-ar21.svg" alt="linux" height="45"/> </a> 
<a href="https://flask.palletsprojects.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/wiki/netbox-community/netbox/images/logos/netbox_logo.svg" alt="linux" height="45"/> </a> 

</p>


## Getting Started
---
[netbox-kea-dhcpv4-server](https://gitlab.com/pet-projects26/netbox-kea-dhcpv4-server) contain my script about learning and creating Netbox integration with Kea DHCPv4 Server 

Purpose of theses file is to run an Flask app as a webhook listener and process event sent from Netbox


## Prerequisites
---
In order to run run these file your may need following requirements
* 1 machine running Netbox
* 1 machine running Flask app
* Connection between two machine
* Python3 


## 🛠️ Installation Steps
---
1. Clone the repository
```bash
git clone https://gitlab.com/pet-projects26/netbox-kea-dhcpv4-server.git
```

2. Change working directory
```bash
cd netbox-kea-dhcpv4-server/ 
```

3. Install Python dependencies
```bash
pip3 install -r requirement
```

4. Run Flask app
```
python3 main.py
```

5. Configure Netbox webhook on Netbox with IP Address of Flask app
6. Create `DHCP` tag on Netbox

    All ranges create must have `DHCP` tag included that have been created above or it will not be written to in configuration file

7. Perform CRUD operations in IPAM - IP Ranges menu

    After perform CRUD operations python listener will create a file call `dhcp-dhcp.json`


`dhcp-dhcp.json` need to be import into Kea DHCPv4 server and restart to take affect
You can refer to [Kea](https://kea.readthedocs.io/en/kea-2.0.1/arm/config.html?highlight=include#configuration-files-inclusion) for more information

## Contributing

Any contribute please create a branch, add commits, and open a pull request or contact via email.

It's good to know if this repo helpful.

## Authors
[G](https://github.com/haryHuds0n) , [Harry](https://github.com/haryHuds0n) , [GiaHung](https://github.com/haryHuds0n)
