import json
import re
import os

# Recreate pool
def subnet_pool(start_address , end_address):
  
    start_address = re.sub('/.*', '', start_address)
    end_address = re.sub('/.*', '', end_address)
    pool = f'{{"pool": "{start_address} - {end_address}" }}'
    pool = json.loads(f"[ {pool} ]")

    return pool

# Recreate gateway
def gateway(gateway, name_server):
    
    gw = f'{{"name": "routers", "data": "{gateway}" }}, {{"name": "domain-name-servers", "data": "{name_server}" }} '
    
    return json.loads(f"[ {gw} ]")  

def get_data(data):
    
    subnets_and_pools = {}
    subnets_and_pools['subnet'] = data['custom_fields']['subnet']
    subnets_and_pools['pools'] = subnet_pool(data['start_address'], data['end_address'])
    subnets_and_pools['option-data'] = gateway(data['custom_fields']['default_gateway'], data['custom_fields']['name_server'])
    
    return  subnets_and_pools
    
def extract(request):

  # Extract data from response
  data = request['data']
  model = request['model']
  
  if len(data['tags']) != 0:
    tag = data['tags'][0]['slug']
  else:
    tag = ''
  
  result = []
  subnets_and_pools = {}
  filename = f"dhcp-{tag}.json"
  
  
  if model == 'iprange' and tag == 'dhcp':
        
      # Handle initial created event
    if request['event'] == 'created':
      
      # Create file if file not exist
      if not os.path.isfile(f'./{filename}'):
        
        subnets_and_pools = get_data(data)
        result.append(subnets_and_pools)
        
        with open(filename, 'w') as outfile:
          json.dump(result, outfile, indent=4)
      
      else:
        #Handle create more range
        with open(filename, 'r+') as outfile:
          
          content = json.load(outfile)
          subnets_and_pools = get_data(data)
      
          content.append(subnets_and_pools)
          print(content)
          
        with open(filename, 'w') as outfile:
          json.dump(content, outfile, indent=4)
    
    #Handle updated event
    if request['event'] == 'updated':
      with open(filename, 'r') as outfile:
        content = json.load(outfile)
        
        #Update content in file match with data on Netbox
        for dict in content:
          # Check if subnet in current file match pre subnet on Netbox
          if dict['subnet'] == request['snapshots']['prechange']['custom_fields']['subnet']:
            dict['subnet'] = data['custom_fields']['subnet']
            dict['pools'] = subnet_pool(data['start_address'], data['end_address'])
            dict['option-data'] = gateway(data['custom_fields']['default_gateway'], data['custom_fields']['name_server'])
      
      with open(filename, 'w') as outfile:
        json.dump(content, outfile, indent=4)
        
    #Handle deleted event
    if request['event'] == 'deleted':
      with open(filename, 'r') as outfile:
        content = json.load(outfile)
        subnet = data['custom_fields']['subnet']
        for dict in content:
          if dict['subnet'] == subnet:
            content.remove(dict)
      
      with open(filename, 'w') as outfile:
        json.dump(content, outfile, indent=4)